import {
    StyleSheet,
    Text,
    View,
    Dimensions,
    PanResponder,
    Button
} from 'react-native'
import React, { useRef, useState, useEffect } from 'react'

const CountMoviment = () => {

  const [count, setCount] = useState(0);
  const screenHeight = Dimensions.get('window').height;
  const gestureThreshold = screenHeight * 0.25;

  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove:(event, gestureState) =>{},
      onPanResponderRelease:(event, gestureState) =>{
        if(gestureState.dy < -gestureThreshold){
          setCount((prevCount)=> prevCount+1)
        }
      }
    })
  ).current

  return (
    <View style={styles.container} {...panResponder.panHandlers}>
      <Text>Valor do contador: {count}</Text>
    </View>
  )
}

export default CountMoviment;

const styles = StyleSheet.create({
  container:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
})