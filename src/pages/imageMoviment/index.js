import { StyleSheet, Text, View, Dimensions, Image, PanResponder } from 'react-native';
import React, { useRef, useState, useEffect } from 'react';

const ImageMoviment = () => {
    const screenWidth = Dimensions.get('window').width;
    const [direction, setDirection] = useState(0);
    const images = [
        'https://classic.exame.com/wp-content/uploads/2019/03/filme-dumbo.jpg?quality=70&strip=info&w=1200',
        'https://i.ytimg.com/vi/aqGSEk_KqL8/mqdefault.jpg',
        'https://img.quizur.com/f/img63b3485092d035.03451113.jpg?lastEdited=1672693867',
        'https://i.pinimg.com/originals/85/4f/8d/854f8d7552f3d3c752338e4b12ab6ffa.jpg',
        'https://media.licdn.com/dms/image/C5103AQEtqvjoecRJOw/profile-displayphoto-shrink_800_800/0/1516680395417?e=2147483647&v=beta&t=xZN--zpxzrTiGdrW40Joo-ZC3bjMSepNFLd129-AnDQ',
        'https://media.licdn.com/dms/image/C5603AQG-zEflzGjyrg/profile-displayphoto-shrink_400_400/0/1516787062505?e=2147483647&v=beta&t=OL3MdbjEL-tpHpYpzOTRc7LKNG1-tySYCl87np4iJho',
    ];

    const panResponder = useRef(
        PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onPanResponderMove: (event, gestureState) => {

            },
            onPanResponderRelease: (event, gestureState) => {
                const gestureThreshold = screenWidth * 0.35;
                if (gestureState.dx < -gestureThreshold) {
                    setDirection((prevDirection) => (prevDirection + 1) % images.length);
                } else if (gestureState.dx > gestureThreshold) {
                    setDirection((prevDirection) => (prevDirection - 1 + images.length) % images.length);
                }
            },
        })
    ).current;

    return (
        <View style={styles.container}>
            <Image
                source={{ uri: images[direction] }}
                style={styles.image}
                {...panResponder.panHandlers}
            />
        </View>
    );
};

export default ImageMoviment;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        width: 300,
        height: 300, 
        resizeMode: 'cover',
        borderRadius: 8
    }
})